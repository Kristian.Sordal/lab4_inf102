package INF102.lab4.sorting;

import java.util.List;
import java.util.Collections;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int size = list.size();

        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                T first = list.get(j);
                T second = list.get(j + 1);

                if (first.compareTo(second) > 0) {
                    Collections.swap(list, j, j + 1);
                }
            }
        }
    }
}