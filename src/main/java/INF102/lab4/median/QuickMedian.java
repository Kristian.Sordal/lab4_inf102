package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
// import java.util.Collections;

public class QuickMedian implements IMedian {
    private <T extends Comparable<T>> T medianListModify(List<T> listCopy, int kth, int pivotIndex) {
        int size = listCopy.size();

        // --- base case
        if (size == 1) {
            return listCopy.get(0);
        }

        List<T> smaller = new ArrayList<>();
        List<T> greater = new ArrayList<>();

        T pivot = listCopy.get(pivotIndex);
        for (T t : listCopy) {
            if (t.compareTo(pivot) <= 0) {
                smaller.add(t);
            } else {
                greater.add(t);
            }
        }

        Random rand = new Random();
        if (kth < smaller.size()) {
            pivotIndex = rand.nextInt(smaller.size());
            return medianListModify(smaller, kth, pivotIndex);
        } else {
            pivotIndex = rand.nextInt(greater.size());
            return medianListModify(greater, kth - smaller.size(), pivotIndex);
        }
    }

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list

        // --- return a method which allows for modifying the copy of the list
        int medianIndex = list.size() / 2;
        Random rand = new Random();
        int pivotIndex = rand.nextInt(list.size()); 
        return medianListModify(listCopy, medianIndex, pivotIndex);
    }

    // public static void main(String[] args) {
    //     QuickMedian qm = new QuickMedian();

    //     List<Integer> list = new ArrayList<>();

    //     list.add(9);
    //     list.add(1);
    //     list.add(0);
    //     list.add(2);
    //     list.add(3);
    //     list.add(4);
    //     list.add(6);
    //     list.add(8);
    //     list.add(7);
    //     list.add(10);
    //     list.add(5);

    //     // [0,1,2,3,4,5,6,7,8,9,10]

    //     System.out.println("Median: " + qm.median(list));
    // }
}